## Conventions de nommage

**Classes**
Exemple : ClasseJava

**Méthodes**
Exemple : MethodeJava

**Attributs**
Exemple attribut privé : attributJava
Exemple attribut publique : attributJava
Exemple final : ATTRIBUT_JAVA

**Fichers**
Exemple : ficher_cree.*

**Dossiers**
Exemple : DossierCree

**Attributs HTML/CSS**
Exemple : navbar-acceuil

Nous devons avoir des noms explicites.
Commentaires uniquement si nécessaire.

Une personne est nécessaire avant la validation d'une merge request.
Pour le nom des branches : jean-michel/nom-de-fonctionnalite