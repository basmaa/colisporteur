package usmb.info806.colisporteur.models;

import org.junit.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import static org.junit.Assert.*;

public class PasswordCreationFormTest {

    @Test
    public void checkMatchingPasswords() {
        PasswordCreationForm validPasswordCreationForm = new PasswordCreationForm(1, "aze");
        String password = "ceci est un test";

        validPasswordCreationForm.setPassword(password);
        validPasswordCreationForm.setConfirmPassword(password);
        validPasswordCreationForm.checkMatchingPasswords();

        assertTrue(new BCryptPasswordEncoder().matches(validPasswordCreationForm.getConfirmPassword(), validPasswordCreationForm.getPassword()));


        PasswordCreationForm invalidPasswordCreationForm = new PasswordCreationForm(1, "aze");

        invalidPasswordCreationForm.setPassword("ceci est un test");
        invalidPasswordCreationForm.setConfirmPassword("ceci est un test échoué");
        invalidPasswordCreationForm.checkMatchingPasswords();

        assertNull(invalidPasswordCreationForm.getConfirmPassword());


        PasswordCreationForm invalidPasswordCreationForm1 = new PasswordCreationForm(1, "aze");

        invalidPasswordCreationForm1.setPassword("a");
        invalidPasswordCreationForm1.setConfirmPassword("b");
        invalidPasswordCreationForm1.checkMatchingPasswords();

        assertNull(invalidPasswordCreationForm1.getConfirmPassword());
    }

    @Test
    public void checkStringLength() {
        PasswordCreationForm validPasswordCreationForm = new PasswordCreationForm(1, "aze");
        StringBuilder validPassword = new StringBuilder();
        while(validPassword.length() < PasswordCreationForm.MINIMUM_PASSWORD_SIZE)
            validPassword.append("a");

        validPasswordCreationForm.setPassword(validPassword.toString());

        assertTrue(new BCryptPasswordEncoder().matches(validPassword.toString(), validPasswordCreationForm.getPassword()));


        PasswordCreationForm invalidPasswordCreationForm = new PasswordCreationForm(1, "aze");
        StringBuilder invalidPassword = new StringBuilder();
        while(invalidPassword.length() < PasswordCreationForm.MINIMUM_PASSWORD_SIZE - 1)
            invalidPassword.append("a");

        invalidPasswordCreationForm.setPassword(invalidPassword.toString());

        assertEquals("", invalidPasswordCreationForm.getPassword());
    }
}