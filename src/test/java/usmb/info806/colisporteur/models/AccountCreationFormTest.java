package usmb.info806.colisporteur.models;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.*;

public class AccountCreationFormTest {
    @Before
    public void setUp() {
        DbContext.accountsList = new ArrayList<>();

        AccountCreationForm accountCreationForm = new AccountCreationForm();

        accountCreationForm.setPseudo("Bob");
        accountCreationForm.setEmail("bob@gmail.com");
        accountCreationForm.setPhone("0102030405");

        DbContext.accountsList.add(new Account(1, accountCreationForm));
    }

    @Test
    public void pseudoAlreadyExists() {
        AccountCreationForm validAccountForm = new AccountCreationForm();
        validAccountForm.setPseudo("Bobby");

        assertFalse(validAccountForm.pseudoAlreadyExists());


        AccountCreationForm invalidAccountForm = new AccountCreationForm();
        invalidAccountForm.setPseudo("Bob");

        assertTrue(invalidAccountForm.pseudoAlreadyExists());
    }

    @Test
    public void emailAlreadyExists() {
        AccountCreationForm validAccountForm = new AccountCreationForm();
        validAccountForm.setEmail("Bobby@gmail.com");

        assertFalse(validAccountForm.emailAlreadyExists());


        AccountCreationForm invalidAccountForm = new AccountCreationForm();
        invalidAccountForm.setEmail("bob@gmail.com");

        assertTrue(invalidAccountForm.emailAlreadyExists());
    }

    @Test
    public void phoneAlreadyExists() {
        AccountCreationForm validAccountForm = new AccountCreationForm();
        validAccountForm.setPhone("0102034405");

        assertFalse(validAccountForm.phoneAlreadyExists());


        AccountCreationForm invalidAccountForm = new AccountCreationForm();
        invalidAccountForm.setPhone("0102030405");

        assertTrue(invalidAccountForm.phoneAlreadyExists());
    }

    @Test
    public void isBirthdayValid() {
        Calendar majorDate = Calendar.getInstance();
        majorDate.setTime(new Date());
        majorDate.add(Calendar.YEAR, -40);

        AccountCreationForm validAccountForm = new AccountCreationForm();
        validAccountForm.setBirthday(majorDate.getTime());

        assertTrue(validAccountForm.isBirthdayValid());


        Calendar sameDateAsMajor = Calendar.getInstance();
        sameDateAsMajor.setTime(new Date());
        sameDateAsMajor.add(Calendar.YEAR, -AccountCreationForm.MAJORITY_AGE);

        AccountCreationForm validAccountForm1 = new AccountCreationForm();
        validAccountForm1.setBirthday(sameDateAsMajor.getTime());

        assertTrue(validAccountForm1.isBirthdayValid());


        Calendar minorDate = Calendar.getInstance();
        minorDate.setTime(new Date());
        minorDate.add(Calendar.YEAR, -5);

        AccountCreationForm invalidAccountForm = new AccountCreationForm();
        invalidAccountForm.setBirthday(minorDate.getTime());

        assertFalse(invalidAccountForm.isBirthdayValid());


        Calendar sameDateAsMajorPlusOne = Calendar.getInstance();
        sameDateAsMajorPlusOne.setTime(new Date());
        sameDateAsMajorPlusOne.add(Calendar.YEAR, -AccountCreationForm.MAJORITY_AGE);
        sameDateAsMajorPlusOne.add(Calendar.DATE, 1);

        AccountCreationForm invalidAccountForm1 = new AccountCreationForm();
        invalidAccountForm1.setBirthday(sameDateAsMajorPlusOne.getTime());

        assertFalse(invalidAccountForm1.isBirthdayValid());
    }

    @After
    public void tearDown() {
        int lastIndex = DbContext.accountsList.size() - 1;
        DbContext.accountsList.remove(lastIndex);
    }
}