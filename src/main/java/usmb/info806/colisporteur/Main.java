package usmb.info806.colisporteur;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import usmb.info806.colisporteur.models.DbContext;

@SpringBootApplication
public class Main {
    public static void main(String[] args) {
        new DbContext();

        SpringApplication.run(Main.class, args);
    }

}
