package usmb.info806.colisporteur.models;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.BindingResult;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class PasswordUpdateForm {
    @NotNull
    private String oldPassword;

    @NotNull
    @Size(min = MINIMUM_PASSWORD_SIZE)
    private String newPassword;

    @Size(min = MINIMUM_PASSWORD_SIZE)
    private String confirmNewPassword;

    private static final String ERROR_UNMATCHING_OLD_PASSWORD = "Le mot de passe actuel saisi est invalide !";
    private static final String ERROR_UNMATCHING_PASSWORDS = "Les mots de passe saisis ne sont pas identiques !";
    private static final int MINIMUM_PASSWORD_SIZE = 6;

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = checkStringLength(newPassword);
        checkMatchingPasswords();
    }

    public String getConfirmNewPassword() {
        return confirmNewPassword;
    }

    public void setConfirmNewPassword(String confirmNewPassword) {
        this.confirmNewPassword = confirmNewPassword;
        checkMatchingPasswords();
    }

    public PasswordUpdateForm() {
    }

    public void checkInformationValidity(BindingResult bindingResult, String oldPassword) {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

        if(!encoder.matches(this.oldPassword, oldPassword))
            bindingResult.rejectValue("oldPassword", "error." + this.getClass().getSimpleName(), ERROR_UNMATCHING_OLD_PASSWORD);

        if(this.confirmNewPassword == null)
            bindingResult.rejectValue("confirmNewPassword", "error." + this.getClass().getSimpleName(), ERROR_UNMATCHING_PASSWORDS);
    }

    private void checkMatchingPasswords() {
        if (this.newPassword != null && this.confirmNewPassword != null) {
            if (!new BCryptPasswordEncoder().matches(this.confirmNewPassword, this.newPassword) || this.newPassword.equals(""))
                this.confirmNewPassword = null;
        }
    }

    private String checkStringLength(String stringToCheck) {
        return stringToCheck.length() < MINIMUM_PASSWORD_SIZE ? "" : new BCryptPasswordEncoder().encode(stringToCheck);
    }
}
