package usmb.info806.colisporteur.models;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.validation.BindingResult;

import javax.validation.constraints.*;
import java.sql.SQLOutput;
import java.util.Date;

public class Ride {

    private int id;
    @NotBlank
    @Size(min = 3, max = 30)
    private String destination;
    @Size(min = 3, max = 30)
    @NotBlank
    private String startingPoint;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @NotNull
    private Date departureDate;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @NotNull
    private Date arrivalDate;
    @NotNull
    @Min(1)
    @Max(24)
    private double weight;
    @NotNull
    @Min(1)
    @Max(99)
    private double length;
    @NotNull
    @Min(1)
    @Max(99)
    private double width;
    @NotNull
    @Min(1)
    @Max(99)
    private double height;
    @NotNull
    @Min(4)
    @Max(10)
    private double price;
    @NotBlank
    private String description;
    private Account creator;

    private static final double MAXIMUM_DIMENSION = 99;
    private static final double DEFAULT_DIMENSION = 1;
    private static final double MAXIMUM_WEIGHT = 24;
    private static final double DEFAULT_WEIGHT = 1;
    private static final double MAXIMUM_PRICE = 10;
    private static final double DEFAULT_PRICE = 4;
    private static final String ERROR_DESTINATION = "La destination et le point de départ ne doivent pas être identique !";
    private static final String ERROR_DATE_DEPARTURE = "La date de départ est dépassé !";
    private static final String ERROR_DATE_ARRIVAL = "La date d'arrivée ne doit pas être avant la date de départ !";
    private static final String ERROR_LENGTH = "Vérifiez les dimensions longueur !";
    private static final String ERROR_WIDTH = "Vérifiez les dimensions longueur, largeur et hauteur  !";
    private static final String ERROR_HEIGHT = "Vérifiez les dimensions longueur, largeur et hauteur  !";
    private static final String ERROR_PRICE = "Le Prix proposé est trop élevé  !";
    private static final String ERROR_WEIGHT = "Vous dépassez le poids maximum authorisé  !";

    public Ride() {
        this.price = DEFAULT_PRICE;
        this.weight = DEFAULT_WEIGHT;
        this.length = DEFAULT_DIMENSION;
        this.width = DEFAULT_DIMENSION;
        this.height = DEFAULT_DIMENSION;
        this.departureDate = new Date();
        this.arrivalDate = new Date();
    }

    Ride(int id, String destination, String startingPoint, Date departureDate, Date arrivalDate, double weight, double length, double width, double height
            , double price, String description, Account creator) {
        this.id = id;
        this.destination = destination;
        this.startingPoint = startingPoint;
        this.departureDate = departureDate;
        this.arrivalDate = arrivalDate;
        this.weight = weight;
        this.length = length;
        this.width = width;
        this.height = height;
        this.price = price;
        this.description = description;
        this.creator = creator;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getStartingPoint() {
        return startingPoint;
    }

    public void setStartingPoint(String startingPoint) {
        this.startingPoint = startingPoint;
    }

    public Date getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }

    public Date getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(Date arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Account getCreator() {
        return creator;
    }

    public void setCreator(Account creator) {
        this.creator = creator;
    }

    private boolean checkDepartureDate() {
        return this.departureDate.equals(new Date());
    }

    private boolean checkArrivalDate() {
        return this.arrivalDate.before(this.departureDate);
    }

    private boolean isNotTheSame() {
        return this.startingPoint.equals(this.destination);
    }

    private boolean moreThanMaxHeight() {
        return this.height > MAXIMUM_DIMENSION;
    }

    private boolean moreThanMaxWidth() {
        return this.width > MAXIMUM_DIMENSION;
    }

    private boolean moreThanMaxLength() {
        return this.length > MAXIMUM_DIMENSION;
    }

    private boolean moreThanMaxPrice() {
        return this.price > MAXIMUM_PRICE;
    }

    private boolean moreThanMaxWeight() {
        return this.weight > MAXIMUM_WEIGHT;
    }

    public void checkInformationValidity(BindingResult bindingResult) {
        String classError = "error." + this.getClass().getName();
        if (checkDepartureDate())
            bindingResult.rejectValue("departureDate", classError, ERROR_DATE_DEPARTURE);
        if (checkArrivalDate()) bindingResult.rejectValue("arrivalDate", classError, ERROR_DATE_ARRIVAL);
        if (isNotTheSame()) bindingResult.rejectValue("destination", classError, ERROR_DESTINATION);
        if (moreThanMaxHeight()) bindingResult.rejectValue("height", classError, ERROR_HEIGHT);
        if (moreThanMaxWidth()) bindingResult.rejectValue("width", classError, ERROR_WIDTH);
        if (moreThanMaxLength()) bindingResult.rejectValue("length", classError, ERROR_LENGTH);
        if (moreThanMaxPrice())
            bindingResult.rejectValue("price", classError, ERROR_PRICE);
        if (moreThanMaxWeight()) bindingResult.rejectValue("weight", classError, ERROR_WEIGHT);
    }
}
