package usmb.info806.colisporteur.models;

import java.util.UUID;

class RandomStringGenerator {
    static String generateString() {
        String randomString = UUID.randomUUID().toString();

        return randomString.replace("-", "");
    }
}
