package usmb.info806.colisporteur.models;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.awt.image.DataBuffer;
import java.util.ArrayList;
import java.util.Date;

public class DbContext {
    static String emailAddress = "colisporteur@gmail.com";
    static String emailPassword = "coliscolis";

    public static ArrayList<Account> accountsList;
    public static ArrayList<Ride> ridesList;

    public DbContext() {
        setAccountsList();
        setRidesList();
    }

    public static Account searchAccount(String userPseudo){
        return accountsList.stream().filter(a -> a.getPseudo().equals( userPseudo) ).findAny().orElse(null);
    }

    private void setAccountsList() {
        accountsList = new ArrayList<>();

        accountsList.add(new Account(1, "Bobby", "booby.lafont@gmail.com",
                new BCryptPasswordEncoder().encode("bonjour"), "Lafont", "Booby",
                "15 place de la roue", "", "73000", "Chambéry", "0450608090",
                "15/02/1995", null));
        accountsList.add(new Account(3, "Pascal", "pascalito0123@gmail.com",
                new BCryptPasswordEncoder().encode("aurevoir"), "Barreau", "Pascal",
                "3515 route du vermont", "Le grenier", "01550", "Rouage",
                "0506070809", "10/10/1961", null));

        accountsList.add(new Account(4, "Pascool", "pascalitoooo0123@gmail.com", "Barreau",
                "Pascal", "3515 route du vermont", "", "01550", "Rouage",
                "0684848484", "15/10/1960", "yo"));
    }
    private void setRidesList() {
        ridesList = new ArrayList<>();

        ridesList.add(new Ride(1, "Paris", "Chambéry", new Date(), new Date(),1.5, 1.5, 1.3,1.2
            , 1.0, " ", DbContext.accountsList.get(1)));
        ridesList.add(new Ride(2, "Paris", "Chambéry", new Date(), new Date(),1.5, 1.5,1.5,1.3
                , 1.0, " ", DbContext.accountsList.get(0)));
    }


}
