package usmb.info806.colisporteur.models;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

class InscriptionMessage {

    static MimeMessage draftEmailMessage(Session mailSession, Account account) {
        String emailSubject = "Inscription à ColisPorteur";

        String emailBody = "Bonjour,\n\nVeuillez cliquer le lien suivant pour confirmer votre inscription :\n"
                + "http://localhost:8080/account/inscriptionConfirmation?id=" + account.getId() + "&token="
                + account.getConfirmAccountToken();

        MimeMessage emailMessage = new MimeMessage(mailSession);

        try {
            emailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(account.getEmail()));

            emailMessage.setSubject(emailSubject);

            emailMessage.setText(emailBody);
        } catch (MessagingException e) {
            e.printStackTrace();
        }

        return emailMessage;
    }
}
