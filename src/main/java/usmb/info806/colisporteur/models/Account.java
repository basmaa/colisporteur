package usmb.info806.colisporteur.models;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Account {
    private int id;
    private String pseudo;
    private String email;
    private String password;
    private String lastName;
    private String firstName;
    private String addressL1;
    private String addressL2;
    private String postalCode;
    private String city;
    private String phone;
    private Date birthday;
    private String confirmAccountToken;

    public int getId() {
        return id;
    }

    public String getPseudo() {
        return pseudo;
    }

    String getEmail() {
        return email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return this.password;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getAddressL1() {
        return addressL1;
    }

    public String getAddressL2() {
        return addressL2;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public String getCity() {
        return city;
    }

    String getPhone() {
        return phone;
    }

    public Date getBirthday() {
        return birthday;
    }

    public String getConfirmAccountToken() {
        return confirmAccountToken;
    }

    public void confirmAccount() {
        this.confirmAccountToken = null;
    }

    public Account(int id, AccountCreationForm accountCreationForm) {
        this.id = id;
        this.confirmAccountToken = RandomStringGenerator.generateString();

        this.pseudo = accountCreationForm.getPseudo();
        this.email = accountCreationForm.getEmail();
        this.firstName = accountCreationForm.getFirstName();
        this.lastName = accountCreationForm.getLastName();
        this.addressL1 = accountCreationForm.getAddressL1();
        this.addressL2 = accountCreationForm.getAddressL2();
        this.postalCode = accountCreationForm.getPostalCode();
        this.city = accountCreationForm.getCity();
        this.phone = accountCreationForm.getPhone();
        this.birthday = accountCreationForm.getBirthday();
    }

    /*
    The following methods are made for the Account management while the DataBase is not set.
     */

    Account(int id, String pseudo, String email, String password, String lastName, String firstName, String addressL1,
            String addressL2, String postalCode, String city, String phone, String birthday, String confirmAccountToken) {
        this.id = id;
        this.pseudo = pseudo;
        this.email = email;
        this.password = password;
        this.lastName = lastName;
        this.firstName = firstName;
        this.addressL1 = addressL1;
        this.addressL2 = addressL2;
        this.postalCode = postalCode;
        this.city = city;
        this.phone = phone;
        setBirthday(birthday);
    }

    Account(int id, String pseudo, String email, String lastName, String firstName, String addressL1, String addressL2,
            String postalCode, String city, String phone, String birthday, String confirmAccountToken) {
        this.id = id;
        this.pseudo = pseudo;
        this.email = email;
        this.lastName = lastName;
        this.firstName = firstName;
        this.addressL1 = addressL1;
        this.addressL2 = addressL2;
        this.postalCode = postalCode;
        this.city = city;
        this.phone = phone;
        setBirthday(birthday);
        this.confirmAccountToken = confirmAccountToken;
    }

    private void setBirthday(String birthday) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        try {
            this.birthday = formatter.parse(birthday);
        } catch (ParseException e) {
            this.birthday = null;
        }
    }

    public void updateAccount(AccountUpdateForm updateAccount){
        this.pseudo = updateAccount.getPseudo();
        this.email = updateAccount.getEmail();
        this.lastName = updateAccount.getLastName();
        this.firstName = updateAccount.getFirstName();
        this.addressL1 = updateAccount.getAddressL1();
        this.addressL2 = updateAccount.getAddressL2();
        this.postalCode = updateAccount.getPostalCode();
        this.city = updateAccount.getCity();
        this.phone = updateAccount.getPhone();
        this.birthday = updateAccount.getBirthday();
    }
}
