package usmb.info806.colisporteur.models;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.validation.BindingResult;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Calendar;
import java.util.Date;

public class AccountUpdateForm {
    private int id;

    @NotBlank
    private String pseudo;

    @NotBlank
    private String email;

    @NotBlank
    private String lastName;

    @NotBlank
    private String firstName;

    @NotBlank
    private String addressL1;

    private String addressL2;

    @NotBlank
    private String postalCode;

    @NotBlank
    private String city;

    @NotBlank
    private String phone;

    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date birthday;


    private static final String ERROR_PSEUDO = "Ce pseudonyme est déjà utilisé !";
    private static final String ERROR_EMAIL = "Cet e-mail est déjà utilisé !";
    private static final String ERROR_PHONE = "Ce numéro de téléphone est déjà utilisé !";
    private static final String ERROR_INVALID_BIRTHDAY = "Vous devez être majeur pour pouvoir créer un compte !";

    public AccountUpdateForm(Account userAccount) {
        this.id = userAccount.getId();
        this.pseudo = userAccount.getPseudo();
        this.email = userAccount.getEmail();
        this.lastName = userAccount.getLastName();
        this.firstName = userAccount.getFirstName();
        this.addressL1 = userAccount.getAddressL1();
        this.addressL2 = userAccount.getAddressL2();
        this.postalCode = userAccount.getPostalCode();
        this.city = userAccount.getCity();
        this.phone = userAccount.getPhone();
        this.birthday = userAccount.getBirthday();
    }

    public AccountUpdateForm() {
    }

    public int getId() {
        return id;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getAddressL1() {
        return addressL1;
    }

    public void setAddressL1(String addressL1) {
        this.addressL1 = addressL1;
    }

    public String getAddressL2() {
        return addressL2;
    }

    public void setAddressL2(String addressL2) {
        this.addressL2 = addressL2;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    private final int MAJORITY_AGE = 18;

    private Date getMajorDate() {
        Calendar majorDate = Calendar.getInstance();
        majorDate.setTime(new Date());

        majorDate.add(Calendar.YEAR, -MAJORITY_AGE);
        majorDate.add(Calendar.DATE, 1);

        return majorDate.getTime();
    }

    public void checkInformationValidity(BindingResult bindingResult, Account authentification) {
        String errorClassName = "error." + this.getClass().getSimpleName();
        if (!authentification.getPseudo().equals(this.pseudo)) {
            if (pseudoAlreadyExists())
                bindingResult.rejectValue("pseudo", "error." + errorClassName, ERROR_PSEUDO);
        }
        if (!authentification.getEmail().equals(this.email)) {
            if (emailAlreadyExists())
                bindingResult.rejectValue("email", "error." + errorClassName, ERROR_EMAIL);
        }
        if (!authentification.getPhone().equals(this.phone)) {
            if (phoneAlreadyExists())
                bindingResult.rejectValue("phone", "error." + errorClassName, ERROR_PHONE);
        }
        if (birthday != null) {
            if (!isBirthdayValid())
                bindingResult.rejectValue("birthday", "error." + errorClassName, ERROR_INVALID_BIRTHDAY);
        }
    }

    private boolean pseudoAlreadyExists() {
        return DbContext.accountsList.stream().anyMatch(account -> account.getPseudo().equals(this.pseudo));
    }

    private boolean emailAlreadyExists() {
        return DbContext.accountsList.stream().anyMatch(account -> account.getEmail().equals(this.email));
    }

    private boolean phoneAlreadyExists() {
        return DbContext.accountsList.stream().anyMatch(account -> account.getPhone().equals(this.phone));
    }

    private boolean isBirthdayValid() {
        return birthday.before(getMajorDate());
    }


}
