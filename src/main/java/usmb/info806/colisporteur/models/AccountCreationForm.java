package usmb.info806.colisporteur.models;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.validation.BindingResult;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Calendar;
import java.util.Date;

public class AccountCreationForm {
    @NotBlank
    private String pseudo;

    @NotBlank
    private String email;

    @NotBlank
    private String lastName;

    @NotBlank
    private String firstName;

    @NotBlank
    private String addressL1;

    private String addressL2;

    @NotBlank
    private String postalCode;

    @NotBlank
    private String city;

    @NotBlank
    private String phone;

    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date birthday;

    @NotNull
    private boolean acceptConditions;

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getAddressL1() {
        return addressL1;
    }

    public void setAddressL1(String addressL1) {
        this.addressL1 = addressL1;
    }

    public String getAddressL2() {
        return addressL2;
    }

    public void setAddressL2(String addressL2) {
        this.addressL2 = addressL2;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public boolean isAcceptConditions() {
        return acceptConditions;
    }

    public void setAcceptConditions(boolean acceptConditions) {
        this.acceptConditions = acceptConditions;
    }

    private static final String ERROR_PSEUDO = "Ce pseudonyme est déjà utilisé !";
    private static final String ERROR_EMAIL = "Cet e-mail est déjà utilisé !";
    private static final String ERROR_PHONE = "Ce numéro de téléphone est déjà utilisé !";
    private static final String ERROR_INVALID_BIRTHDAY = "Vous devez être majeur pour pouvoir créer un compte !";
    private static final String ERROR_ACCEPT_CONDITIONS_REFUSED = "Vous devez accepter les conditions d'utilisation !";

    static final int MAJORITY_AGE = 18;

    public AccountCreationForm() {
    }

    private Date getMajorDate() {
        Calendar majorDate = Calendar.getInstance();
        majorDate.setTime(new Date());

        majorDate.add(Calendar.YEAR, -MAJORITY_AGE);
        majorDate.add(Calendar.DATE, 1);

        return majorDate.getTime();
    }

    public void checkInformationValidity(BindingResult bindingResult) {
        String errorClassName = "error." + this.getClass().getSimpleName();

        if (pseudoAlreadyExists())
            bindingResult.rejectValue("pseudo", "error." + errorClassName, ERROR_PSEUDO);

        if (emailAlreadyExists())
            bindingResult.rejectValue("email", "error." + errorClassName, ERROR_EMAIL);

        if (phoneAlreadyExists())
            bindingResult.rejectValue("phone", "error." + errorClassName, ERROR_PHONE);

        if (birthday != null) {
            if (!isBirthdayValid())
                bindingResult.rejectValue("birthday", "error." + errorClassName, ERROR_INVALID_BIRTHDAY);
        }

        if (!isAcceptConditions())
            bindingResult.rejectValue("acceptConditions", "error." + errorClassName, ERROR_ACCEPT_CONDITIONS_REFUSED);
    }

    boolean pseudoAlreadyExists() {
        return DbContext.accountsList.stream().anyMatch(account -> account.getPseudo().equals(this.pseudo));
    }

    boolean emailAlreadyExists() {
        return DbContext.accountsList.stream().anyMatch(account -> account.getEmail().equals(this.email));
    }

    boolean phoneAlreadyExists() {
        return DbContext.accountsList.stream().anyMatch(account -> account.getPhone().equals(this.phone));
    }

    boolean isBirthdayValid() {
        return birthday.before(getMajorDate());
    }
}
