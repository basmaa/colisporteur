package usmb.info806.colisporteur.models;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.BindingResult;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class PasswordCreationForm {
    private int accountId;
    private String accountToken;

    @NotNull
    @Size(min = MINIMUM_PASSWORD_SIZE)
    private String password;

    @Size(min = MINIMUM_PASSWORD_SIZE)
    private String confirmPassword;

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public String getAccountToken() {
        return accountToken;
    }

    public void setAccountToken(String accountToken) {
        this.accountToken = accountToken;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = checkStringLength(password);
        checkMatchingPasswords();
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
        checkMatchingPasswords();
    }

    private static final String ERROR_UNMATCHING_PASSWORDS = "Les mots de passe saisis ne sont pas identiques !";
    static final int MINIMUM_PASSWORD_SIZE = 6;

    public PasswordCreationForm(int accountId, String accountToken) {
        this.accountId = accountId;
        this.accountToken = accountToken;
    }

    public void checkInformationValidity(BindingResult bindingResult) {
        if (this.confirmPassword == null)
            bindingResult.rejectValue("confirmPassword", "error." + this.getClass().getSimpleName(), ERROR_UNMATCHING_PASSWORDS);
    }

    void checkMatchingPasswords() {
        if (this.password != null && this.confirmPassword != null) {
            if (!new BCryptPasswordEncoder().matches(this.confirmPassword, this.password) || this.password.equals(""))
                this.confirmPassword = null;
        }
    }

    private String checkStringLength(String stringToCheck) {
        return stringToCheck.length() < MINIMUM_PASSWORD_SIZE ? "" : new BCryptPasswordEncoder().encode(stringToCheck);
    }
}
