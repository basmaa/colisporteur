package usmb.info806.colisporteur.models;

import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.MimeMessage;

public class EmailSession {

    private final Session _mail_session;

    private static final String MAIL_SMTP_PORT = "mail.smtp.port";
    private static final String PORT = "587";

    private static final String MAIL_SMTP_AUTH = "mail.smtp.auth";

    private static final String MAIL_SMTP_STARTTLS = "mail.smtp.starttls.enable";

    public EmailSession() {
        _mail_session = setMailServerSession();
    }

    public void sendInscriptionEmail(Account recipient) {
        try (Transport transport = _mail_session.getTransport("smtp")) {
            transport.connect("smtp.gmail.com", DbContext.emailAddress, DbContext.emailPassword);

            MimeMessage emailMessage = InscriptionMessage.draftEmailMessage(_mail_session, recipient);

            transport.sendMessage(emailMessage, emailMessage.getAllRecipients());
        } catch (MessagingException ex) {
            Logger.getLogger(EmailSession.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private Session setMailServerSession() {
        Properties emailSessionProperties = System.getProperties();

        emailSessionProperties.put(MAIL_SMTP_PORT, PORT);
        emailSessionProperties.put(MAIL_SMTP_AUTH, "true");
        emailSessionProperties.put(MAIL_SMTP_STARTTLS, "true");

        return Session.getDefaultInstance(emailSessionProperties, null);
    }
}
