package usmb.info806.colisporteur.controllers;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import usmb.info806.colisporteur.models.Account;
import usmb.info806.colisporteur.models.DbContext;
import usmb.info806.colisporteur.models.Ride;

import javax.validation.Valid;
import java.util.Calendar;
import java.util.Date;
import java.util.stream.Collectors;


@Controller
public class RideController {
    @GetMapping("/ride/offerARide")
    public String getOfferARideForm(Model model) {
        model.addAttribute("ride", new Ride());
        return "/ride/offerARide";
    }


    @PostMapping("/ride/offerARide")
    public String checkPersonInfo(@Valid Ride ride, BindingResult bindingResult, Authentication authentication) {
        ride.setArrivalDate(redefineArrivalDate(ride.getArrivalDate()));

        ride.checkInformationValidity(bindingResult);
        if (bindingResult.hasErrors())
            return "/ride/offerARide";
        else {
            ride.setId(DbContext.ridesList.get(DbContext.ridesList.size() - 1).getId() + 1);

            Account creator = DbContext.accountsList.stream().filter(a -> a.getPseudo().equals(authentication.getName())).findAny().orElse(null);
            ride.setCreator(creator);

            DbContext.ridesList.add(ride);
            return "redirect:/";
        }
    }

    private Date redefineArrivalDate(Date date) {
        Calendar arrivalDate = Calendar.getInstance();
        arrivalDate.setTime(date);
        arrivalDate.add(Calendar.HOUR, 2);

        return arrivalDate.getTime();
    }

    @GetMapping("/ride/list")
    public String getRidesList(Model model) {
        model.addAttribute("ridesList", DbContext.ridesList.stream().filter(x -> x.getArrivalDate().after(getTodaysDate())).collect(Collectors.toList()));
        return "ride/ridesList";
    }

    private Date getTodaysDate() {
        Calendar date = Calendar.getInstance();
        date.setTime(new Date());
        date.set(Calendar.HOUR_OF_DAY, 0);
        date.set(Calendar.MINUTE, 0);
        date.set(Calendar.SECOND, 0);

        return date.getTime();
    }

    @GetMapping("/ride/details/{rideId}")
    public String getRideDetails(@PathVariable int rideId, Model model) {
        Ride ride = DbContext.ridesList.stream().filter(r -> r.getId() == rideId).findAny().orElse(null);

        if(ride == null || ride.getDepartureDate().before(getTodaysDate()))
            return "redirect:/";
        else
            model.addAttribute("ride", ride);
            return "ride/rideDetails";
    }
}
