package usmb.info806.colisporteur.controllers;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import usmb.info806.colisporteur.models.Account;
import usmb.info806.colisporteur.models.DbContext;

import org.springframework.validation.BindingResult;

import usmb.info806.colisporteur.models.*;

import javax.validation.Valid;
import java.util.ArrayList;

@Controller
public class AccountController {

    private final InMemoryUserDetailsManager inMemoryUserDetailsManager;

    public AccountController(InMemoryUserDetailsManager inMemoryUserDetailsManager) {
        this.inMemoryUserDetailsManager = inMemoryUserDetailsManager;
    }

    @GetMapping("/account/inscription")
    public String getInscriptionForm(Model model) {
        model.addAttribute("accountCreationForm", new AccountCreationForm());
        return "account/inscription";
    }

    @PostMapping("/account/inscription")
    public String doInscription(@Valid AccountCreationForm accountCreationForm, BindingResult bindingResult) {
        accountCreationForm.checkInformationValidity(bindingResult);

        if (bindingResult.hasErrors())
            return "account/inscription";
        else {
            int unusedDbAccountId = DbContext.accountsList.get(DbContext.accountsList.size() - 1).getId() + 1;

            Account dbAccount = new Account(unusedDbAccountId, accountCreationForm);
            DbContext.accountsList.add(dbAccount);

            EmailSession emailSession = new EmailSession();
            emailSession.sendInscriptionEmail(dbAccount);

            return "account/inscriptionEmailSent";
        }
    }

        @GetMapping("/account/inscriptionConfirmation")
    public String getInscriptionConfirmation(@RequestParam("id") int accountId, @RequestParam("token") String token, Model model) {
        Account account = DbContext.accountsList.stream().filter(a -> a.getId() == accountId).findAny().orElse(null);

        if (account != null) {
            if (token.equals(account.getConfirmAccountToken())) {
                model.addAttribute("passwordCreationForm", new PasswordCreationForm(accountId, token));

                return "account/inscriptionConfirmation";
            }
        }

        return "redirect:/";
    }

    @PostMapping("/account/inscriptionConfirmation")
    public String confirmInscription(@Valid PasswordCreationForm passwordCreationForm, BindingResult bindingResult) {
        Account account = DbContext.accountsList.stream().filter(a -> a.getId() == passwordCreationForm.getAccountId()
                && a.getConfirmAccountToken().equals(passwordCreationForm.getAccountToken())).findAny().orElse(null);

        if (account != null) {
            passwordCreationForm.checkInformationValidity(bindingResult);

            if (bindingResult.hasErrors())
                return "account/inscriptionConfirmation";
            else {
                account.setPassword(passwordCreationForm.getPassword());
                account.confirmAccount();

                String username = account.getPseudo();
                String password = account.getPassword();
                inMemoryUserDetailsManager.createUser(User.withUsername(username).password(password).roles("USER").build());
                
                return "account/inscriptionConfirmed";
            }
        } else
            return "redirect:/";
    }


    @GetMapping("/login")
    public String login() {
        return "/account/login";
    }

    @GetMapping("/login/success")
    public String succLoginPage(Authentication authentication) {
        if (authentication.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_USER")))
            return "/index";
        else if (authentication.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_ADMIN")))
            return "/admin";

        return "/index";
    }
    
    @GetMapping("/account/passwordUpdate")
    public String getPasswordUpdatePage(Model model) {
        model.addAttribute("passwordUpdateForm", new PasswordUpdateForm());
        return "account/passwordUpdate";
    }

    @PostMapping("/account/passwordUpdate")
    public String updatePassword(@Valid PasswordUpdateForm passwordUpdateForm, BindingResult bindingResult, Authentication authentication) {
        Account account = DbContext.accountsList.stream().filter(a -> a.getPseudo().equals(authentication.getName())).findAny().orElse(null);

        if (account != null) {
            passwordUpdateForm.checkInformationValidity(bindingResult, account.getPassword());

            if (bindingResult.hasErrors())
                return "account/passwordUpdate";
            else {
                account.setPassword(passwordUpdateForm.getNewPassword());

                if(inMemoryUserDetailsManager.userExists(authentication.getName()))
                    inMemoryUserDetailsManager.changePassword(account.getPassword(), passwordUpdateForm.getNewPassword());
                else
                    inMemoryUserDetailsManager.createUser(User.withUsername(authentication.getName()).password(passwordUpdateForm.getNewPassword()).roles("USER").build());

                return "account/passwordChanged";
            }
        } else
            return "redirect:/";
    }

    @GetMapping("/account/updateAccount")
    public String getUpdateForm(Model model, Authentication authentication) {
        model.addAttribute("accountUpdateForm", new AccountUpdateForm(DbContext.searchAccount(authentication.getName())));
        return "account/updateAccount";
    }

    @PostMapping("/account/updateAccount")
    public String updateAccount(@Valid AccountUpdateForm accountUpdateForm, BindingResult bindingResult, Authentication authentication) {
        Account account = DbContext.accountsList.stream().filter(a -> a.getPseudo().equals( authentication.getName()) ).findAny().orElse(null);
        accountUpdateForm.checkInformationValidity(bindingResult,account);

        if (bindingResult.hasErrors())
            return "account/updateAccount";
        else
            DbContext.accountsList.get(accountUpdateForm.getId()).updateAccount(accountUpdateForm);
        return "account/updateAccount";
    }
}

