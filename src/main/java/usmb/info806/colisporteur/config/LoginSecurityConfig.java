package usmb.info806.colisporteur.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import usmb.info806.colisporteur.models.Account;
import usmb.info806.colisporteur.models.DbContext;

import java.util.Properties;
import java.util.stream.Collectors;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true) // enables Spring Security pre/post annotations
@EnableAutoConfiguration(exclude = SecurityAutoConfiguration.class)
public class LoginSecurityConfig extends WebSecurityConfigurerAdapter {

    protected void configure(AuthenticationManagerBuilder auth) throws Exception {

        for (Account a : DbContext.accountsList.stream().filter(x -> x.getPassword() != null).collect(Collectors.toList())) {
            auth.inMemoryAuthentication()
                    .withUser(a.getPseudo()).password(a.getPassword()).roles("USER");
        }

        auth.inMemoryAuthentication()
                .withUser("admin").password(passwordEncoder().encode("admin")).roles("ADMIN");

        auth.userDetailsService(inMemoryUserDetailsManager());
    }

    @Bean
    public InMemoryUserDetailsManager inMemoryUserDetailsManager() {
        final Properties users = new Properties();
        users.put("user","pass,ROLE_USER,enabled");
        return new InMemoryUserDetailsManager(users);
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    @Override
    protected AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManager();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http
                .authorizeRequests()
                .antMatchers("/account/inscription", "/", "/ride/list", "/ride/details/*", "/account/inscriptionConfirmation").permitAll()
                .anyRequest().authenticated()

                .and()

                .formLogin()

                .loginPage("/login")
                .usernameParameter("user-name-email").passwordParameter("password")
                .defaultSuccessUrl("/login/success")
                .permitAll()

                .and()
                .logout()
                .logoutSuccessUrl("/")


                .and()
                .httpBasic();// HTTP Basic Authentication is supported


        http.csrf().disable(); // cross-site request forgery

    }

}
